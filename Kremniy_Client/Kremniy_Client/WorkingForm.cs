﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kremniy_Client
{
    public partial class WorkingForm : Form
    {
        public WorkingForm()
        {
            InitializeComponent();
        }

        private void нормированиеТрудозатратToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Hide();
            RationingForm rationingForm = new RationingForm();
            rationingForm.ShowDialog();
            Show();
        }

        private void аТППToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Hide();
            ATPPForm atppForm = new ATPPForm();         
            atppForm.ShowDialog();
            Show();
        }

        private void планированиеПроизводстваToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Hide();
            PlanningForm planningForm = new PlanningForm();
            planningForm.ShowDialog();
            Show();
        }
    }
}
