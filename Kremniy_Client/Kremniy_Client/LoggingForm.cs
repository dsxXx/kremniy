﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kremniy_Client
{
    public partial class LoggingForm : Form
    {
        public LoggingForm()
        {
            InitializeComponent();
            WindowState = FormWindowState.Maximized;
            
            LogInButton.Location = new Point(DisplayRectangle.Width / 2 - (LogInButton.Size.Width/2), DisplayRectangle.Height / 2 - (LogInButton.Size.Height / 2) + 100);
            userNameLabel.Location = new Point(DisplayRectangle.Width / 2 - (userNameLabel.Size.Width / 2), DisplayRectangle.Height / 2 - (userNameLabel.Size.Height / 2) - 75);
            userNameTextBox.Location = new Point(DisplayRectangle.Width / 2 - (userNameTextBox.Size.Width / 2), DisplayRectangle.Height / 2 - (userNameTextBox.Size.Height / 2) - 50);
            passwordLabel.Location = new Point(DisplayRectangle.Width / 2 - (passwordLabel.Size.Width / 2), DisplayRectangle.Height / 2 - (passwordLabel.Size.Height / 2) - 25);
            passwordTextBox.Location = new Point(DisplayRectangle.Width / 2 - (passwordTextBox.Size.Width / 2), DisplayRectangle.Height / 2 - (passwordTextBox.Size.Height / 2));
            //Console.ReadLine();
        }

        private void LogInButton_MouseClick(object sender, MouseEventArgs e)
        {
            if (userNameTextBox.Text != null && passwordTextBox.Text != null && !userNameTextBox.Text.Equals("") && !passwordTextBox.Text.Equals(""))
            {
                Hide();
                WorkingForm workingForm = new WorkingForm();
                //на данный момент заглушка для активации второй формы
                workingForm.ShowDialog();
                //Show();
            }
            else
            {
                MessageBox.Show("Данные введены неверно", "Ошибка ебац",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }
    }
}
