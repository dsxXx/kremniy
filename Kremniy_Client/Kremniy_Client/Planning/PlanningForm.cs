﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;


namespace Kremniy_Client
{
    
    public partial class PlanningForm : Form
    {
        private ContextMenuStrip strip;
        private ToolStripMenuItem done = new ToolStripMenuItem();
        private ToolStripMenuItem working = new ToolStripMenuItem();
        private DataGridViewCellEventArgs mouseLocation;
        int rowIndex, columnindex;

        /// <summary>
        /// список названий оснастки (4Ш..., 4Пф..,, ...)
        /// </summary>
        public static List<String> units = new List<string>();

        /// <summary>
        /// экземпляр для работы с EXCEL
        /// </summary>
        Excel.Application ex = new Microsoft.Office.Interop.Excel.Application();

        /// <summary>
        /// Список деталей оснастки
        /// </summary>
        List<String> namesOfParts = new List<string>();

        /// <summary>
        /// Конструктор
        /// </summary>
        public PlanningForm()
        {
            InitializeComponent();
            addUnit("4Пф654");//добавил для теста
            RefreshUnitsBox();
            unitGrid.ReadOnly = false;
            unitGrid.DefaultCellStyle.Font = new System.Drawing.Font("Times new roman", 8);
            unitGrid.CurrentCell = null;
            addContextMenu();
        }

        /// <summary>
        /// обновляет список оснастки (удаляет и перезаписывает по новой в случае обновления количества оснастки)
        /// </summary>
        public void RefreshUnitsBox()
        {
            unitsBox.Items.Clear();
            for (int i = 0; i < units.Count; i++)
            {
                unitsBox.Items.Add(units[i]);
            }
        }

        /// <summary>
        /// добавляет в массив новую оснастку
        /// </summary>
        /// <param name="name"></param>
        public void addUnit(String name)
        {
            units.Add(name);
            RefreshUnitsBox();
        }


        /// <summary>
        /// Закрывает процессы EXCEL
        /// </summary>
        public void CloseProcess()
        {
            Process[] List;
            List = Process.GetProcessesByName("EXCEL");
            foreach (Process proc in List)
            {
                proc.Kill();
            }
            GC.Collect();
        }

        private void unitsBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            parseExcel(unitsBox.GetItemText(unitsBox.SelectedItem));
        }

        
        private void PlanningForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            //ex.Workbooks.Close();
            //ex.Quit();
            //GC.Collect();
            //System.Runtime.InteropServices.Marshal.FinalReleaseComObject(ex);
            CloseProcess();
            Application.Exit();
            
        }

        /// <summary>
        /// Ищет конец операций в тех.ведомости;
        /// </summary>
        /// <param name="appl"></param>
        /// <param name="sh"></param>
        /// <returns></returns>
        public int findEndOfOperations(Excel.Application appl, Excel.Worksheet sh)
        {
            for (int i=1; i < 50; i++)
            {
                for (int j=1; j < 50; j++)
                {
                    if (sh.Cells[i, j].Text == "№ осн")
                    {
                        return j;
                    }
                }
            }
            return 0;
        }

        /// <summary>
        /// Поиск и занесение в массив деталей
        /// </summary>
        /// <param name="sh"></param>
        /// <param name="name"></param>
        public void findParts(Excel.Worksheet sh, String name)
        {
            for (int i = 1; i < 300; i++)
            {
                try
                {
                    if (sh.Cells[i, 1].Text != "" && sh.Cells[i, 1].Text == name)
                    {
                        String item = sh.Cells[i, 1].Text + "." + sh.Cells[i, 2].Text + " " + sh.Cells[i, 3].Text;
                        namesOfParts.Add(item);
                    }
                }
                catch (Exception exc)
                {
                    String excMessage = exc.Message;
                }
            }
        }

        /// <summary>
        /// Устанавливает размеры таблицы
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        public void setGridSize(int rows, int columns)
        {
            unitGrid.RowCount = rows;
            unitGrid.ColumnCount = columns;
            unitGrid.Size = new System.Drawing.Size(unitGrid.ColumnCount * unitGrid.Columns[0].Width + 50, namesOfParts.Count * unitGrid.Rows[0].Height + 50);
        }

        /// <summary>
        /// Устанавливает ширину столбцов таблицы
        /// </summary>
        /// <param name="width"></param>
        public void setGridColumnsWidth(int width)
        {
            for (int i = 0; i < unitGrid.ColumnCount; i++)
            {
                unitGrid.Columns[i].Width = width;
            }
        }

        /// <summary>
        /// Заполняет таблицу
        /// </summary>
        /// <param name="endOfOperations"></param>
        /// <param name="sh"></param>
        public void fillUnitGrid(int endOfOperations, Excel.Worksheet sh, String name)
        {
            int rowCounter = 0;
            for (int i = 1; rowCounter < namesOfParts.Count; i++)
            {
                if (sh.Cells[i, 1].Text == name)
                {
                    for (int j = 1; j <= endOfOperations; j++)
                    {
                        unitGrid.Rows[rowCounter].Cells[j - 1].Value = sh.Cells[i, j].Text;
                    }
                    rowCounter++;
                }
            }
        }

        /// <summary>
        /// Освободить ресурсы после работы
        /// </summary>
        public void terminateExcelProcesses()
        {
            ex.Workbooks.Close();
            ex.Quit();
            GC.Collect();
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(ex);
        }


        /// <summary>
        /// Функция парсит файл тех.ведомости и выводит ее в unitGrid
        /// </summary>
        /// <param name="name"></param>
        public void parseExcel(String name)
        {
            // Выяснить как получать ссылки на тех.ведомость оснастки
            ex.Workbooks.Open(@"D:\Kremniy production planning\Kremniy_Client\Kremniy_Client\654 TEH VED.xlsm",
                   Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                   Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                   Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                   Type.Missing, Type.Missing);
            Excel.Worksheet sheet = (Excel.Worksheet)ex.Worksheets.get_Item(1);

            findParts(sheet, name);

            setGridSize(namesOfParts.Count, 25);

            setGridColumnsWidth(70);

            fillUnitGrid(findEndOfOperations(ex, sheet), sheet, name);

            terminateExcelProcesses();
        }

        public void addContextMenu()
        {
            strip = new ContextMenuStrip();
            working.Text = "В работе";
            done.Text = "Выполнено";
            working.Click += new EventHandler(working_Click);
            done.Click += new EventHandler(done_Click);
            strip.Items.Add(working);
            strip.Items.Add(done);
            foreach (DataGridViewColumn column in unitGrid.Columns)
            {

                column.ContextMenuStrip = strip;
                column.ContextMenuStrip.Items.Add(done);
                column.ContextMenuStrip.Items.Add(working);
            }
        }

        // Change the cell's color.
        private void working_Click(object sender, EventArgs args)
        {
            unitGrid.Rows[rowIndex]
                .Cells[columnindex].Style.BackColor
                = Color.Yellow;
            unitGrid.CurrentCell = null;
        }
        private void done_Click(object sender, EventArgs args)
        {
            unitGrid.Rows[rowIndex]
                .Cells[columnindex].Style.BackColor
                = Color.Green;
            unitGrid.CurrentCell = null;
        }

        
        

        private void unitGrid_MouseClick(object sender, MouseEventArgs e)
        {
            
            strip.Show(unitGrid, unitGrid.PointToClient(Cursor.Position));

        }

        private void unitGrid_CellContextMenuStripNeeded(object sender, DataGridViewCellContextMenuStripNeededEventArgs e)
        {
            strip = new ContextMenuStrip();
            if (strip == null)
            {
                working.Text = "В работе";
                done.Text = "Выполнено";
                strip.Items.Add(working);
                strip.Items.Add(done);
            }
            e.ContextMenuStrip = strip;
            e.ContextMenuStrip.Show();
        }

        private void unitGrid_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            rowIndex = e.RowIndex;
            columnindex = e.ColumnIndex;
        }
    }
}
