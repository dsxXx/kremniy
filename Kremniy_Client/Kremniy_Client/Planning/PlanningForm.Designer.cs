﻿namespace Kremniy_Client
{
    partial class PlanningForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.unitsBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.unitGrid = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.unitGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // unitsBox
            // 
            this.unitsBox.FormattingEnabled = true;
            this.unitsBox.Location = new System.Drawing.Point(12, 37);
            this.unitsBox.Name = "unitsBox";
            this.unitsBox.Size = new System.Drawing.Size(121, 21);
            this.unitsBox.TabIndex = 0;
            this.unitsBox.SelectedIndexChanged += new System.EventHandler(this.unitsBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Шифр единицы";
            // 
            // unitGrid
            // 
            this.unitGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.unitGrid.Location = new System.Drawing.Point(158, 37);
            this.unitGrid.Name = "unitGrid";
            this.unitGrid.Size = new System.Drawing.Size(240, 150);
            this.unitGrid.TabIndex = 2;
            this.unitGrid.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler(this.unitGrid_CellContextMenuStripNeeded);
            this.unitGrid.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.unitGrid_CellMouseEnter);
            this.unitGrid.MouseClick += new System.Windows.Forms.MouseEventHandler(this.unitGrid_MouseClick);
            // 
            // PlanningForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.unitGrid);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.unitsBox);
            this.Name = "PlanningForm";
            this.Text = "Планирование";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.PlanningForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.unitGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox unitsBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView unitGrid;
    }
}