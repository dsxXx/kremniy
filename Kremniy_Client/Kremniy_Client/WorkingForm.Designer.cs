﻿namespace Kremniy_Client
{
    partial class WorkingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.модульToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.планированиеПроизводстваToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.нормированиеТрудозатратToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.аТППToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.модульToolStripMenuItem,
            this.справкаToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // модульToolStripMenuItem
            // 
            this.модульToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.планированиеПроизводстваToolStripMenuItem,
            this.нормированиеТрудозатратToolStripMenuItem,
            this.аТППToolStripMenuItem});
            this.модульToolStripMenuItem.Name = "модульToolStripMenuItem";
            this.модульToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.модульToolStripMenuItem.Text = "Модуль";
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.справкаToolStripMenuItem.Text = "Справка";
            // 
            // планированиеПроизводстваToolStripMenuItem
            // 
            this.планированиеПроизводстваToolStripMenuItem.Name = "планированиеПроизводстваToolStripMenuItem";
            this.планированиеПроизводстваToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.планированиеПроизводстваToolStripMenuItem.Text = "Планирование производства";
            this.планированиеПроизводстваToolStripMenuItem.Click += new System.EventHandler(this.планированиеПроизводстваToolStripMenuItem_Click);
            // 
            // нормированиеТрудозатратToolStripMenuItem
            // 
            this.нормированиеТрудозатратToolStripMenuItem.Name = "нормированиеТрудозатратToolStripMenuItem";
            this.нормированиеТрудозатратToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.нормированиеТрудозатратToolStripMenuItem.Text = "Нормирование трудозатрат";
            this.нормированиеТрудозатратToolStripMenuItem.Click += new System.EventHandler(this.нормированиеТрудозатратToolStripMenuItem_Click);
            // 
            // аТППToolStripMenuItem
            // 
            this.аТППToolStripMenuItem.Name = "аТППToolStripMenuItem";
            this.аТППToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.аТППToolStripMenuItem.Text = "АТПП";
            this.аТППToolStripMenuItem.Click += new System.EventHandler(this.аТППToolStripMenuItem_Click);
            // 
            // WorkingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.Name = "WorkingForm";
            this.Text = "Кремний";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem модульToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem планированиеПроизводстваToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem нормированиеТрудозатратToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem аТППToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
    }
}