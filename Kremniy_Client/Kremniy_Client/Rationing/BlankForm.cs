﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kremniy_Client.Rationing
{
    public partial class BlankForm : Form
    {
        RationingForm rf;
        public BlankForm(RationingForm f)
        {
            InitializeComponent();
            rf = f;
        }


        public void button1_Click(object sender, EventArgs e)
        {
            rf.textBox1.Text = textBox1.Text;
            rf.textBox2.Text = textBox2.Text;
            rf.textBox3.Text = textBox3.Text;
            rf.textBox4.Text = comboBox1.Text;

            rf.groupBox1.Visible = true;
            rf.button1.Visible = false;
            rf.button2.Visible = true;
            rf.Show();
            Close();
        }
    }
}
