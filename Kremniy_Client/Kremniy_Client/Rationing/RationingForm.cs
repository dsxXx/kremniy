﻿
using Kremniy_Client.Rationing;
using System;
using System.Windows.Forms;

namespace Kremniy_Client
{
    public partial class RationingForm : Form
    {

        public RationingForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BlankForm blankForm = new BlankForm(this);
            blankForm.ShowDialog();
        }

        private void RationingForm_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Hide();
            OperationForm operationForm = new OperationForm(this);
            operationForm.ShowDialog();
        }
    }
}
