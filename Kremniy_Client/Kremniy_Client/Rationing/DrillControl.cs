﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kremniy_Client.Rationing
{
    public partial class DrillControl : UserControl
    {
        public DrillControl(string title)
        {
            InitializeComponent();
            groupBox1.Text = title;
        }
    }
}
