﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Kremniy_Client.Rationing
{
    public partial class OperationForm : Form
    {

        RationingForm rf;
        List<DrillControl> drillControls = new List<DrillControl>();

        public OperationForm(RationingForm f)
        {
            InitializeComponent();
            rf = f;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int posx=12, posy=200;

            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    DrillControl dc = new DrillControl("1 группа отверстий");
                    drillControls.Add(dc);
                    if (drillControls.Count == 1)
                    { 
                        dc.Location = new Point(posx, posy);
                    }
                    else
                    {
                        posy += 120;
                        dc.Location = new Point(posx, posy);
                    }                    

                    rf.Controls.Add(dc);
                    break;

            }
            

            rf.Show();
            Close();
        }
    }
}
